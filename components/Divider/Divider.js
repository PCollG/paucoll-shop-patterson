import styles from "../Divider/Divider.module.scss";

const Divider = () => {

    return (
        <div className={styles.divider} />
    );
};

export default Divider;