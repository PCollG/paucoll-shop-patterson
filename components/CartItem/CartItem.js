import Image from "next/image";
import Link from "next/link";

import { useContext } from "react";
import { CartContext, MAX_AMOUNT_PER_ITEM } from "../../context/cart";


import styles from './CartItem.module.scss';

const CartItem = ({id, title, price, image}) => {
    const { dispatch } = useContext(CartContext);

    return (
        <div className={styles.cartItem}>
            <Image
                src={image}
                alt="Patterson Agency Logo"
                width={150}
                height={210}
            />
            <div>{title}</div>
            <div>{price}€</div>
            <div className={styles.cartItem__icon}>
                <a onClick={() => dispatch({ type: "REMOVE_ITEM", payload: { id } })}>
                    <Image
                        src="/../public/images/trash.svg"
                        alt="Trash"
                        width={20}
                        height={20}
                    />
                </a>
            </div>
        </div>
    );
};

export default CartItem;