import styles from './Breadcrumb.module.scss';

const Breadcrumb = () => {

    return (
        <div>
            <ul className={styles.breadcrumb__list}>
                <li>Todos los productos</li>
                <li>Cocina</li>
                <li>Productos Destacados</li>
            </ul>
        </div>
    );
};

export default Breadcrumb;