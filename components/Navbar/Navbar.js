import styles from './Navbar.module.scss';
import Wrapper from "../../components/Wrapper/Wrapper";
import Logo from "../../components/Logo/Logo";
import Cart from "../../components/Cart/Cart";

const Navbar = () => {
    return (
        <nav>
            <Wrapper>
                <div className={styles.navbar}>
                    <Logo />
                    <Cart />
                </div>
            </Wrapper>
        </nav>
    );
};

export default Navbar;
