import styles from "../Title/Title.module.scss";

const Title = ({text}) => {

    return (
        <div className={styles.title}>{text}</div>
    );
};

export default Title;