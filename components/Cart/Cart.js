import Image from "next/image";
import Link from "next/link";

import styles from './Cart.module.scss';

import { CartContext } from "../../context/cart";
import { useContext } from "react";


const Cart = () => {

    const { state: items } = useContext(CartContext);
    const amount = items.reduce((acc, cur) => acc + cur.amount, 0);

    return (
        <div className={styles.cart}>
            <Link href="/cart">
                <a className={styles.cart__icon}>
                    <Image
                        src="/../public/images/carrito.svg"
                        alt="Shopping Cart"
                        width={32}
                        height={32}
                    />
                    <div className={styles.cart__qtyBubble}>{amount}</div>
                </a>
            </Link>
            <div className={styles.cart__text}>
                <div className={styles.cart__price}>0 €</div>
                <Link href="/cart">
                    <a>Mi Carrito</a>
                </Link>
            </div>
        </div>
    );
};

export default Cart;