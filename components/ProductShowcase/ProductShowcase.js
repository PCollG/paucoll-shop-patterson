import styles from './ProductShowcase.module.scss';
import Wrapper from "../../components/Wrapper/Wrapper";
import Title from "../../components/Title/Title";
import Product from "../../components/Product/Product";
import Breadcrumb from "../../components/Breadcrumb/Breadcrumb";

import React from 'react'
import 'keen-slider/keen-slider.min.css'
import { useKeenSlider } from 'keen-slider/react'

const ProductShowcase = ({ data }) => {
    const [currentSlide, setCurrentSlide] = React.useState(0)

    const [sliderRef, slider] = useKeenSlider({
        slidesPerView: 4,
        spacing: 40,
        loop: true,
        initial: 0,
        slideChanged(s) {
            setCurrentSlide(s.details().relativeSlide)
        },
    })

    return (
        <section className={styles.productShowcase}>
            <Wrapper>
                <div className={styles.productShowcase__titleCont}>
                    <Breadcrumb />
                    <Title text={'Productos Destacados'} />
                </div>
                <div ref={sliderRef} className="keen-slider">
                    {data && data.map((product) => {
                        return (
                            <Product key={product.id} {...product}/>
                        )
                    })}
                </div>
                {slider && (
                    <>
                        <ArrowLeft
                            onClick={(e) => e.stopPropagation() || slider.prev()}
                            disabled={currentSlide === 0}
                        />
                        <ArrowRight
                            onClick={(e) => e.stopPropagation() || slider.next()}
                            disabled={currentSlide === slider.details().size - 1}
                        />
                    </>
                )}
            </Wrapper>
        </section>
    );
};

function ArrowLeft(props) {
    return (
        <svg
            onClick={props.onClick}
            className={`${styles.customArrow} ${styles.customArrowLeft}`}
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            fill={`#fff`}
        >
            <path d="M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z" />
        </svg>
    )
}

function ArrowRight(props) {
    return (
        <svg
            onClick={props.onClick}
            className={`${styles.customArrow} ${styles.customArrowRight}`}
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            fill={`#fff`}
        >
            <path d="M5 3l3.057-3 11.943 12-11.943 12-3.057-3 9-9z" />
        </svg>
    )
}


export default ProductShowcase;
