import styles from './Product.module.scss';
import Image from "next/image";

import { useContext } from "react";
import { CartContext } from "../../context/cart";

const Product = ({title, price, image, id}) => {
    const { dispatch } = useContext(CartContext);

    return (
        <div className={`keen-slider__slide ${styles.product}`}>
            <div className={styles.product__imgCont}>
                <Image
                    src={image}
                    alt={title}
                    width={210}
                    height={300}
                />
                <div className={styles.product__buttonsCont}>
                    <div className={styles.product__button}>
                        <a onClick={() => dispatch({ type: "ADD_ITEM", payload: { id } })}>
                            <Image
                                src="/../public/images/carrito.svg"
                                alt="cart"
                                width={20}
                                height={20}
                            />
                        </a>
                    </div>
                    <div className={styles.product__button}>
                        <Image
                            src="/../public/images/ojo.svg"
                            alt="eye"
                            width={20}
                            height={20}
                        />
                    </div>
                </div>
            </div>
            <div className={styles.product__starRating}>
                <Image
                    src="/../public/images/estrella.svg"
                    alt="Estrella"
                    width={20}
                    height={20}
                />
                <Image
                    src="/../public/images/estrella.svg"
                    alt="Estrella"
                    width={20}
                    height={20}
                />
                <Image
                    src="/../public/images/estrella.svg"
                    alt="Estrella"
                    width={20}
                    height={20}
                />
            </div>
            <div>
                <div className={styles.product__title}>{title}</div>
                <div className={styles.product__price}>{price}€</div>
            </div>
        </div>
    );
};

export default Product;
