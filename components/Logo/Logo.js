import Image from "next/image";
import Link from "next/link";

const Logo = () => {

    return (
        <Link href="/">
            <a>
                <Image
                    src="/../public/images/patterson-agency-logo.png"
                    alt="Patterson Agency Logo"
                    width={350}
                    height={124}
                />
            </a>
        </Link>
    );
};

export default Logo;