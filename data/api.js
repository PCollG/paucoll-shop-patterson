export async function withProducts() {
    const res = await fetch('https://fakestoreapi.com/products?limit=4');
    const products = await res.json();

    return {
        products,
    };
}

export const getTotalSum = (products, items) => {
    return items
        .map(({ id, amount }) => ({
            product: products.find((p) => p.id === id),
            amount,
        }))
        .map(({ product: { price }, amount }) => amount * price)
        .reduce((acc, cur) => acc + cur, 0);
};
