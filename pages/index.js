import Head from 'next/head'

import Navbar from '../components/Navbar/Navbar'
import ProductShowcase from '../components/ProductShowcase/ProductShowcase'

import { withProducts } from "../data/api";

export default function Home({products}) {
    return (
        <div>
            <Head>
                <title>Patterson Agency Shop</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <Navbar/>
            <ProductShowcase data={products}/>
        </div>
    )
}

Home.getInitialProps = withProducts;
